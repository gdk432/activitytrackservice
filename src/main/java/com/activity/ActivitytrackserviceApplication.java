package com.activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.activity.config.RibbonConfiguration;
import com.netflix.ribbon.proxy.annotation.Hystrix;

@Hystrix
@EnableCircuitBreaker
@EnableFeignClients
@EnableEurekaClient
@SpringBootApplication
@RibbonClient(value = "activity-loadbalance", configuration = RibbonConfiguration.class)
public class ActivitytrackserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivitytrackserviceApplication.class, args);
	}

}
