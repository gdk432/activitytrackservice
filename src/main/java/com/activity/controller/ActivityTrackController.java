package com.activity.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.activity.dto.AddActivityRequestDto;
import com.activity.dto.FetchActivityResponseDto;
import com.activity.dto.UpdateActivityRequestDto;
import com.activity.service.AddActivityTrackService;
import com.activity.service.DownloadActivityService;
import com.activity.service.FetchActivityTrackService;
import com.activity.service.RemoveActivityTrackService;
import com.activity.service.UpdateActivityTrackService;

@RestController
@Validated

/**
 * 
 * Controller for Activity Track operations
 *
 */
public class ActivityTrackController {

	@Autowired
	AddActivityTrackService addActivityTrackService;

	@Autowired
	FetchActivityTrackService fetchActivityTrackService;

	@Autowired
	RemoveActivityTrackService removeActivityTrackService;

	@Autowired
	UpdateActivityTrackService updateActivityTrackService;
	
	@Autowired
	DownloadActivityService downloadActivityService;

	/**
	 * 
	 * @param addActivityRequestDto adding list of Activities for that employee
	 * @return success message
	 */
	@PostMapping("/addactivity")
	public String addActivity(@Valid @RequestBody List< @Valid AddActivityRequestDto> addActivityRequestDto) {
		return addActivityTrackService.addActivity(addActivityRequestDto);
	}

	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param employeeCode
	 * @return list of Activities for that employee
	 */
	
	@GetMapping("/getallactivity")
	public List<FetchActivityResponseDto> fetchallActivity(@RequestParam int pageNumber, @RequestParam int pageSize) {
		return fetchActivityTrackService.fetchallActivity(pageNumber, pageSize);
	}
	
	@GetMapping("/getactivitywithemployeecode")
	public List<FetchActivityResponseDto> fetchActivityWithEmployeecode(@RequestParam int pageNumber, @RequestParam int pageSize, @RequestParam Long employeeCode) {
		return fetchActivityTrackService.fetchActivityWithEmployeecode(pageNumber, pageSize, employeeCode);
	}
	
	@GetMapping("/getactivitywithdate")
	public List<FetchActivityResponseDto> fetchActivityWithDate(@RequestParam  int pageNumber, @RequestParam int pageSize,
			@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate activityDate) {
		return fetchActivityTrackService.fetchActivityWithDate(pageNumber, pageSize,  activityDate);
	}
	
	@GetMapping("/getallactivitywithFromandtodate")
	public List<FetchActivityResponseDto> fetchActivityWithFromAndToDate(@RequestParam int pageNumber, @RequestParam int pageSize,
			@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate fromDate, @RequestParam LocalDate toDate) {
		return fetchActivityTrackService.fetchActivityWithFromAndToDate(pageNumber, pageSize, fromDate, toDate);
	}
	
	@GetMapping("/getaactivitywithemployeecodeanddate")
	public List<FetchActivityResponseDto> fetchActivityWithEmployeeCodeAndDate(@RequestParam int pageNumber, @RequestParam int pageSize,
			@RequestParam Long employeeCode, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate activityDate ) {
		return fetchActivityTrackService.fetchActivityWithEmployeeCodeAndDate(pageNumber, pageSize, employeeCode, activityDate);
	}
	
	@GetMapping("/getactivitywithemployeecodefromandtodate")
	public List<FetchActivityResponseDto> fetchActivityWithEmployeecodeFromAndTodate(@RequestParam int pageNumber, @RequestParam int pageSize,
			@RequestParam Long employeeCode, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate fromDate, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate toDate) {
		return fetchActivityTrackService.fetchActivityWithEmployeecodeFromAndTodate(pageNumber, pageSize, employeeCode,  fromDate, toDate);
	}

	@GetMapping("/download")
	public ResponseEntity<Resource> downloadActvityReport(@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate activityDate) {
		 String filename = "activity.xls";
		    InputStreamResource file = new InputStreamResource(downloadActivityService.downloadReportforDate(activityDate));
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=activity.xls")
				.contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
				.body(file);
	}
	/**
	 * 
	 * @param id
	 * @return success message
	 */
	@DeleteMapping("/remove")
	public String removeActivityTrack(@RequestParam Long id) {
		return removeActivityTrackService.removeActivityTrack(id);

	}

	/**
	 * 
	 * @param updateActivityRequestDto
	 * @return success message
	 */
	@PatchMapping("/update")
	public String updateActivityTrack(@RequestBody UpdateActivityRequestDto updateActivityRequestDto) {
		return updateActivityTrackService.updateActivityTrack(updateActivityRequestDto);

	}

}
