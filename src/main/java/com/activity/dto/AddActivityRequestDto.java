package com.activity.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class AddActivityRequestDto {

	private Long employeeCode;
	
	@NotEmpty(message = "activityDescription should not be empty")
	@Size(min = 3, max = 1000, message = "activityDescription should be of 1000 characterstics only")
	private String activityDescription;
	
	@NotEmpty(message = "employeeStatus should not be empty")
	@Size(min = 3, max = 15, message = "employeeStatus should be of 15 characterstics only")
	private String activityStatus;

	public Long getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(Long employeeCode) {
		this.employeeCode = employeeCode;
	}

	
	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	public String getActivityStatus() {
		return activityStatus;
	}

	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}

}
