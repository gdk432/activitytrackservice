package com.activity.dto;

public class FetchActivityRequestDto {

	private Long employeeCode;

	public Long getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(Long employeeCode) {
		this.employeeCode = employeeCode;
	}	
	
	
}
