package com.activity.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UpdateActivityRequestDto {

	private Long id;

	@NotEmpty(message = "employeeStatus should not be empty")
	@Size(min = 3, max = 15, message = "employeeStatus should be of 15 characterstics only")
	private String activityStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActivityStatus() {
		return activityStatus;
	}

	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}

}
