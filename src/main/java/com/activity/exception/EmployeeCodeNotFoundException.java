package com.activity.exception;

public class EmployeeCodeNotFoundException extends RuntimeException {

	public EmployeeCodeNotFoundException(String message) {
		super(message);

	}
}
