package com.activity.exception;

import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.activity.dto.ErrorResponsedto;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ErrorResponsedto> handle(ConstraintViolationException constraintViolationException) {
		ErrorResponsedto errorResponseDto = new ErrorResponsedto();
		errorResponseDto.setStatusCode("404 non found");
		Set<ConstraintViolation<?>> violations = constraintViolationException.getConstraintViolations();
		String errorMessage = "";
		if (!violations.isEmpty()) {
			StringBuilder builder = new StringBuilder();
			violations.forEach(violation -> builder.append(" ," + violation.getMessage()));
			errorMessage = builder.toString();
		} else {
			errorMessage = "ConstraintViolationException occured.";
		}
		errorResponseDto.setStatusMessage(errorMessage.substring(2));
		return new ResponseEntity<>(errorResponseDto, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = IdNotFoundException.class)
	public ResponseEntity<ErrorResponsedto> handleException(IdNotFoundException userException) {
		ErrorResponsedto response = new ErrorResponsedto();
		response.setStatusCode("404 non found");
		response.setStatusMessage(userException.getMessage());
		return new ResponseEntity<ErrorResponsedto>(response, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(value = EmployeeCodeNotFoundException.class)
	public ResponseEntity<ErrorResponsedto> handleException(EmployeeCodeNotFoundException employeeException) {
		ErrorResponsedto response = new ErrorResponsedto();
		response.setStatusCode("404 non found");
		response.setStatusMessage(employeeException.getMessage());
		return new ResponseEntity<ErrorResponsedto>(response, HttpStatus.BAD_REQUEST);

	}

	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException argInvalidException,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ErrorResponsedto response = new ErrorResponsedto();
		response.setStatusCode("505 not a valid Credentials");
		String allFieldErrors = argInvalidException.getBindingResult().getFieldErrors().stream()
				.map(e -> e.getDefaultMessage()).collect(Collectors.joining(", "));
		response.setStatusMessage(allFieldErrors);
		return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
	}

}
