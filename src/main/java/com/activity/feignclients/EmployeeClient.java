package com.activity.feignclients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.activity.dto.EmployeeCodesRequestDto;
import com.activity.dto.FetchEmployeeResponceDto;

@FeignClient(name = "http://Employee-SERVICE/employee")

//@FeignClient(value ="employee-service", url = "http://localhost:8026/employee")

public interface EmployeeClient {
	
	@GetMapping("/fetch/empcode")
	public  FetchEmployeeResponceDto fetchEmployee  (@RequestParam Long employeeCode);
	
	@PostMapping("/fetch/empdetails")
	public  List<FetchEmployeeResponceDto>fetchEmployeeDetails(@RequestBody EmployeeCodesRequestDto employeeCodesRequestDto);
	
}








