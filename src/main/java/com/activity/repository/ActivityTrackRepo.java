package com.activity.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.activity.entity.ActivityTrack;

public interface ActivityTrackRepo extends JpaRepository<ActivityTrack, Long> {

	Page<ActivityTrack> findByemployeeCode(Pageable pageable, Long employeeCode);

	Optional<ActivityTrack> findByactivityStatus(String activityStatus);

	List<ActivityTrack> findByactivityDate(LocalDate date);

	Page<ActivityTrack> findByactivityDate(Pageable pageable, LocalDate activityDate);

	//@Query(value = "select u.* From ActivityTrack u where u.activity_date>=:fromDate and u.activity_date<=:toDate", nativeQuery = true)
	@Query("select d from ActivityTrack d where d.activityDate >= :fromDate and d.activityDate <= :toDate")
	Page<ActivityTrack>  findByfromDateandtoDate(Pageable pageable,@Param("fromDate")  LocalDate fromDate,@Param("toDate") LocalDate toDate);

	//@Query(value = "select u.* From ActivityTrack u where u.employee_code>=:employeeCode and u.activity_date<=:activityDate", nativeQuery = true)
	@Query("select d from ActivityTrack d where d.employeeCode = :employeeCode and d.activityDate = :activityDate")
	Page<ActivityTrack> findByemployeeCodeandDate(Pageable pageable, Long employeeCode, LocalDate activityDate);

	//@Query(value = "select u.* From ActivityTrack u where u.employee_code =:employeeCode and u.activity_date>=:fromDate and u.activity_date<=:toDate", nativeQuery = true)
	@Query("select d from ActivityTrack d where d.employeeCode = :employeeCode and d.activityDate >= :fromDate and d.activityDate <= :toDate")
	Page<ActivityTrack> findByemployeeCodewithFromAndTodate(Pageable pageable, Long employeeCode,@Param("fromDate") LocalDate fromDate,
			@Param("toDate") LocalDate toDate);

//	@Query  (value ="select u.* From ActivityTrack u where u.employee_code =:employeeCode and u.activity_description =:activityDescription",nativeQuery = true)
//	List<ActivityTrack> findByemployeeCodeandDate(Long employeeCode, String activityDescription);

}
