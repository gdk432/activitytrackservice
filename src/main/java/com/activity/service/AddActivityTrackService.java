package com.activity.service;

import java.util.List;

import com.activity.dto.AddActivityRequestDto;

public interface AddActivityTrackService {

	String addActivity(List<AddActivityRequestDto> addActivityRequestDto);

}
