package com.activity.service;

import java.io.ByteArrayInputStream;
import java.time.LocalDate;

public interface DownloadActivityService {

	
	ByteArrayInputStream downloadReportforDate(LocalDate activityDate);

}
