package com.activity.service;

import java.time.LocalDate;
import java.util.List;

import com.activity.dto.FetchActivityRequestDto;
import com.activity.dto.FetchActivityResponseDto;
import com.activity.dto.FetchActivityResponseReportDto;

public interface FetchActivityTrackService {

	List<FetchActivityResponseDto> fetchallActivity(int pageNumber, int pageSize);

	List<FetchActivityResponseDto> fetchActivityWithEmployeecode(int pageNumber, int pageSize, Long employeeCode);

	List<FetchActivityResponseDto> fetchActivityWithDate(int pageNumber, int pageSize, LocalDate activityDate);

	List<FetchActivityResponseDto> fetchActivityWithFromAndToDate(int pageNumber, int pageSize, LocalDate fromDate,
			LocalDate toDate);

	List<FetchActivityResponseDto> fetchActivityWithEmployeeCodeAndDate(int pageNumber, int pageSize, Long employeeCode,
			LocalDate activityDate);

	List<FetchActivityResponseDto> fetchActivityWithEmployeecodeFromAndTodate(int pageNumber, int pageSize,
			Long employeeCode, LocalDate fromDate, LocalDate toDate);

}
