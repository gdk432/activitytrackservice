package com.activity.service;

public interface RemoveActivityTrackService {

	String removeActivityTrack(Long id);

}
