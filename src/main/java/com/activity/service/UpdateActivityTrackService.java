package com.activity.service;

import com.activity.dto.AddActivityRequestDto;
import com.activity.dto.UpdateActivityRequestDto;

public interface UpdateActivityTrackService {

	String updateActivityTrack(UpdateActivityRequestDto updateActivityRequestDto);

}
