package com.activity.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.activity.dto.AddActivityRequestDto;
import com.activity.dto.FetchEmployeeResponceDto;
import com.activity.entity.ActivityTrack;
import com.activity.exception.EmployeeCodeNotFoundException;
import com.activity.feignclients.EmployeeClient;
import com.activity.repository.ActivityTrackRepo;
import com.activity.service.AddActivityTrackService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class AddActivityTrackServiceImpl implements AddActivityTrackService {

	@Autowired
	ActivityTrackRepo activityTrackRepo;

	@Autowired
	EmployeeClient employeeClient;

	/**
	 * 
	 * @param addActivityRequestDto
	 * @return success message
	 */

	@Override
	@HystrixCommand(fallbackMethod = "test")
	public String addActivity(List<AddActivityRequestDto> addActivityRequestDto) {
		List<ActivityTrack> activityList = new ArrayList<>();
		addActivityRequestDto.stream().forEach(actdto -> {

			try {
				FetchEmployeeResponceDto fetchEmployeeResponseDto = new FetchEmployeeResponceDto();
				fetchEmployeeResponseDto = employeeClient.fetchEmployee(actdto.getEmployeeCode());

			} catch (Exception e) {

				throw new EmployeeCodeNotFoundException("InValid Employee Code");

			}

		});

		addActivityRequestDto.stream().forEach(actvityreqdto -> {
			ActivityTrack activityTrack = new ActivityTrack();
			BeanUtils.copyProperties(actvityreqdto, activityList);
			LocalDate localDate = LocalDate.now();
			activityTrack.setActivityDate(localDate);
			activityTrack.setActivityDescription(actvityreqdto.getActivityDescription());
			activityTrack.setActivityStatus(actvityreqdto.getActivityStatus());
			activityTrack.setEmployeeCode(actvityreqdto.getEmployeeCode());
			activityList.add(activityTrack);

		});

		activityTrackRepo.saveAll(activityList);
		return "Activity Succsfully Addedd";
	}

	public String test(List<AddActivityRequestDto> addActivityRequestDto) {
		return "employee service is currently in maintainance please try after some time";
		

	}

}
