package com.activity.service.impl;

import java.io.ByteArrayInputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.activity.dto.ActvityReportRespone;
import com.activity.dto.EmployeeCodesRequestDto;
import com.activity.dto.FetchEmployeeResponceDto;
import com.activity.entity.ActivityTrack;
import com.activity.feignclients.EmployeeClient;
import com.activity.repository.ActivityTrackRepo;
import com.activity.service.DownloadActivityService;
import com.activity.utils.ExcelConvertionUtil;

@Service
public class DownloadActivityServiceImpl implements DownloadActivityService{

	@Autowired
	ActivityTrackRepo activityTrackRepo;
	
	@Autowired
	EmployeeClient employeeClinet;

	@Override
	public ByteArrayInputStream downloadReportforDate(LocalDate activityDate) {
		
		List<ActivityTrack> activityList = activityTrackRepo.findByactivityDate(activityDate);
		
		EmployeeCodesRequestDto employeeCodesRequestDto = new EmployeeCodesRequestDto();
		employeeCodesRequestDto.setEmployeeStatus("Active");
		
		List<Long> employeeCodes = new ArrayList<>();
		activityList.stream().forEach(actvity->{
			employeeCodes.add(actvity.getEmployeeCode());
		});
		employeeCodesRequestDto.setEmployeeCode(employeeCodes);
		List<FetchEmployeeResponceDto> FetchEmployeeResponceDtoList = employeeClinet.fetchEmployeeDetails(employeeCodesRequestDto);
		List<ActvityReportRespone> actvityReportResponeList = new ArrayList<>();
		
		activityList.stream().forEach(activityresponse->{
			
			FetchEmployeeResponceDtoList.stream().forEach(employeeresponse->{
				
				if(activityresponse.getEmployeeCode().equals(employeeresponse.getEmployeeCode())) {
					
					ActvityReportRespone actvityReportRespone = new ActvityReportRespone();
				    BeanUtils.copyProperties(activityresponse, actvityReportRespone);
					actvityReportRespone.setEmployeeName(employeeresponse.getEmployeeName());
					actvityReportResponeList.add(actvityReportRespone);
				}
			});
		});
		ByteArrayInputStream acts = ExcelConvertionUtil.actvitystoExcel(actvityReportResponeList);
		
		return acts;
	}

}


