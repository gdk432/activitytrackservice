package com.activity.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.activity.dto.FetchActivityResponseDto;
import com.activity.dto.FetchActivityResponseReportDto;
import com.activity.dto.FetchEmployeeResponceDto;
import com.activity.entity.ActivityTrack;
import com.activity.exception.EmployeeCodeNotFoundException;
import com.activity.feignclients.EmployeeClient;
import com.activity.repository.ActivityTrackRepo;
import com.activity.service.FetchActivityTrackService;

@Service
public class FetchActivityTrackServiceImpl implements FetchActivityTrackService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FetchActivityTrackServiceImpl.class);

	@Autowired
	ActivityTrackRepo activityTrackRepo;

	@Autowired
	EmployeeClient employeeClient;

	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param employeeCode
	 * @return list of Activities for that employee
	 */

	@Override
	public List<FetchActivityResponseDto> fetchallActivity(int pageNumber, int pageSize) {
		Page<ActivityTrack> activityTrack;
		List<FetchActivityResponseDto> fetchActivityResponseDtoList = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		activityTrack = activityTrackRepo.findAll(pageable);

		LOGGER.info("exception for employees activity");
		if (activityTrack.isEmpty()) {
			LOGGER.info("list is empty");
			throw new EmployeeCodeNotFoundException("No activities found");
		}

		activityTrack.stream().forEach(actTrack -> {
			FetchActivityResponseDto fetchActivityResponseDto = new FetchActivityResponseDto();
			BeanUtils.copyProperties(actTrack, fetchActivityResponseDto);

			fetchActivityResponseDtoList.add(fetchActivityResponseDto);

		});

		return fetchActivityResponseDtoList;
	}

	@Override
	public List<FetchActivityResponseDto> fetchActivityWithEmployeecode(int pageNumber, int pageSize,
			Long employeeCode) {
		Page<ActivityTrack> activityTrack;
		List<FetchActivityResponseDto> fetchActivityResponseDtoList = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		activityTrack = activityTrackRepo.findByemployeeCode(pageable, employeeCode);

		LOGGER.info("exception for employees activity");
		if (activityTrack.isEmpty()) {
			LOGGER.info("list is empty");
			throw new EmployeeCodeNotFoundException("No employee found");
		}

		activityTrack.stream().forEach(actTrack -> {
			FetchActivityResponseDto fetchActivityResponseDto = new FetchActivityResponseDto();
			BeanUtils.copyProperties(actTrack, fetchActivityResponseDto);

			fetchActivityResponseDtoList.add(fetchActivityResponseDto);

		});

		return fetchActivityResponseDtoList;
	}

	@Override
	public List<FetchActivityResponseDto> fetchActivityWithDate(int pageNumber, int pageSize, LocalDate activityDate) {
		Page<ActivityTrack> activityTrack;
		List<FetchActivityResponseDto> fetchActivityResponseDtoList = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		LOGGER.info("got the db result failed to convert the date ");
		activityTrack = activityTrackRepo.findByactivityDate(pageable, activityDate);

		LOGGER.info("got the db result failed to convert the date");
		if (activityTrack.isEmpty()) {
			LOGGER.info("list is empty");
			throw new EmployeeCodeNotFoundException("No activities found for this date");
		}

		activityTrack.stream().forEach(actTrack -> {
			FetchActivityResponseDto fetchActivityResponseDto = new FetchActivityResponseDto();
			BeanUtils.copyProperties(actTrack, fetchActivityResponseDto);

			fetchActivityResponseDtoList.add(fetchActivityResponseDto);

		});

		return fetchActivityResponseDtoList;
	}

	@Override
	public List<FetchActivityResponseDto> fetchActivityWithFromAndToDate(int pageNumber, int pageSize,
			LocalDate fromDate, LocalDate toDate) {
		Page<ActivityTrack> activityTrack;
		List<FetchActivityResponseDto> fetchActivityResponseDtoList = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		activityTrack = activityTrackRepo.findByfromDateandtoDate(pageable, fromDate, toDate);

		LOGGER.info("exception for employees activity");
		if (activityTrack.isEmpty()) {
			LOGGER.info("list is empty");
			throw new EmployeeCodeNotFoundException("No activities found for this period");
		}

		activityTrack.stream().forEach(actTrack -> {
			FetchActivityResponseDto fetchActivityResponseDto = new FetchActivityResponseDto();
			BeanUtils.copyProperties(actTrack, fetchActivityResponseDto);

			fetchActivityResponseDtoList.add(fetchActivityResponseDto);

		});

		return fetchActivityResponseDtoList;
	}

	@Override
	public List<FetchActivityResponseDto> fetchActivityWithEmployeeCodeAndDate(int pageNumber, int pageSize,
			Long employeeCode, LocalDate activityDate) {
		Page<ActivityTrack> activityTrack;
		List<FetchActivityResponseDto> fetchActivityResponseDtoList = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		activityTrack = activityTrackRepo.findByemployeeCodeandDate(pageable, employeeCode, activityDate);

		LOGGER.info("exception for employees activity");
		if (activityTrack.isEmpty()) {
			LOGGER.info("list is empty");
			throw new EmployeeCodeNotFoundException("No employee found");
		}

		activityTrack.stream().forEach(actTrack -> {
			FetchActivityResponseDto fetchActivityResponseDto = new FetchActivityResponseDto();
			BeanUtils.copyProperties(actTrack, fetchActivityResponseDto);

			fetchActivityResponseDtoList.add(fetchActivityResponseDto);

		});

		return fetchActivityResponseDtoList;
	}

	@Override
	public List<FetchActivityResponseDto> fetchActivityWithEmployeecodeFromAndTodate(int pageNumber, int pageSize,
			Long employeeCode, LocalDate fromDate, LocalDate toDate) {
		Page<ActivityTrack> activityTrack;
		List<FetchActivityResponseDto> fetchActivityResponseDtoList = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		activityTrack = activityTrackRepo.findByemployeeCodewithFromAndTodate(pageable, employeeCode, fromDate, toDate);

		LOGGER.info("exception for employees activity");
		if (activityTrack.isEmpty()) {
			LOGGER.info("list is empty");
			throw new EmployeeCodeNotFoundException("No employee found");
		}

		activityTrack.stream().forEach(actTrack -> {
			FetchActivityResponseDto fetchActivityResponseDto = new FetchActivityResponseDto();
			BeanUtils.copyProperties(actTrack, fetchActivityResponseDto);

			fetchActivityResponseDtoList.add(fetchActivityResponseDto);

		});

		return fetchActivityResponseDtoList;
	}
}

