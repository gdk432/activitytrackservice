package com.activity.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.activity.entity.ActivityTrack;
import com.activity.exception.IdNotFoundException;
import com.activity.repository.ActivityTrackRepo;
import com.activity.service.RemoveActivityTrackService;

@Service
public class RemoveActivityTrackServiceImpl implements RemoveActivityTrackService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RemoveActivityTrackServiceImpl.class);
	@Autowired
	ActivityTrackRepo activityTrackRepo;

	/**
	 * 
	 * @param id
	 * @return success message
	 */
	
	@Override
	public String removeActivityTrack(Long id) {
		Optional<ActivityTrack> actTrack = activityTrackRepo.findById(id);
		
		LOGGER.info("exception for id");
				if (!actTrack.isPresent()) {
					LOGGER.info("no activity available");
					throw new IdNotFoundException("Not a valid id");
				}

		ActivityTrack activityTrack = actTrack.get();
		activityTrackRepo.delete(activityTrack);
		return "record status deleted succesully ";
	}

}
