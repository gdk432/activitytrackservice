package com.activity.service.impl;

import java.time.LocalDate;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.activity.dto.UpdateActivityRequestDto;
import com.activity.entity.ActivityTrack;
import com.activity.exception.IdNotFoundException;
import com.activity.repository.ActivityTrackRepo;
import com.activity.service.UpdateActivityTrackService;

@Service
public class UpdateActivityTrackServiceImpl implements UpdateActivityTrackService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateActivityTrackServiceImpl.class);
	@Autowired
	ActivityTrackRepo activityTrackRepo;

	/**
	 * 
	 * @param updateActivityRequestDto
	 * @return success message
	 */
	@Override
	public String updateActivityTrack(UpdateActivityRequestDto updateActivityRequestDto) {
		Optional<ActivityTrack> activityTrackList = activityTrackRepo.findById(updateActivityRequestDto.getId());

		
		LOGGER.info("exception for id");
		if (!activityTrackList.isPresent()) {
			LOGGER.info("id not found");
			throw new IdNotFoundException("Not a valid id");
		}

		ActivityTrack activityTrack = activityTrackList.get();
		activityTrack.setActivityStatus(updateActivityRequestDto.getActivityStatus());
		activityTrack.setActivityDate(LocalDate.now());
		activityTrackRepo.save(activityTrack);

		return "successfully updated";
	}

}
