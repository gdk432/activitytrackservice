package com.activity.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.activity.ActivitytrackserviceApplication;
import com.activity.dto.AddActivityRequestDto;
import com.activity.dto.FetchActivityResponseDto;
import com.activity.service.AddActivityTrackService;
import com.activity.service.DownloadActivityService;
import com.activity.service.FetchActivityTrackService;
import com.activity.service.RemoveActivityTrackService;
import com.activity.service.UpdateActivityTrackService;

@ExtendWith(MockitoExtension.class)
public class ActivityTrackControllerTest extends ActivitytrackserviceApplication {

	@Mock
	RemoveActivityTrackService removeActivityTrackService;

	@Mock
	UpdateActivityTrackService updateActivityTrackService;

	@Mock
	DownloadActivityService downloadActivityService;

	@InjectMocks
	ActivityTrackController activityTrackController;

	// for add activity method
	static AddActivityRequestDto addActivityRequestDto;
	static List<AddActivityRequestDto> addActivityRequestDtoList;
	// static FetchActivityRequestDto fetchActivityRequestDto;
	// static UpdateActivityRequestDto UpdateActivityRequestDto;

	// for fetchallActivity
	static int pageNumber = 0;
	static int pageSize = 10;
	static FetchActivityResponseDto fetchActivityResponseDto;
	static List<FetchActivityResponseDto> fetchActivityResponseDtoList;

	// for fetchActivityWithEmployeecode
	static Long employeeCode = 123L;

	// for fetchActivityWithDate
	static LocalDate activityDate = LocalDate.now();

	// for fetchActivityWithEmployeecodeFromAndTodate
	static LocalDate fromDate = LocalDate.now();
	static LocalDate toDate = LocalDate.now();

	@BeforeAll
	public static void setup() {
		// for addactivity
		addActivityRequestDto = new AddActivityRequestDto();
		addActivityRequestDtoList = new ArrayList<>();
		addActivityRequestDto.setEmployeeCode(123L);
		addActivityRequestDto.setActivityDescription("testing purpose");
		addActivityRequestDto.setActivityStatus("completed");
		addActivityRequestDtoList.add(addActivityRequestDto);

		// for fetch all activity method
		fetchActivityResponseDto = new FetchActivityResponseDto();
		fetchActivityResponseDtoList = new ArrayList<>();
		fetchActivityResponseDto.setEmployeeCode(12345L);
		fetchActivityResponseDto.setId(1L);
		fetchActivityResponseDto.setActivityDate(LocalDate.now());
		fetchActivityResponseDto.setActivityDescription("for test case");
		fetchActivityResponseDto.setActivityStatus("completed");
		fetchActivityResponseDtoList.add(fetchActivityResponseDto);

	}

	@Mock
	AddActivityTrackService addActivityTrackService;

	@Test
	@DisplayName("addActivity")
	public void addActivityTest() {

		when(addActivityTrackService.addActivity(addActivityRequestDtoList)).thenReturn("success");
		String result = activityTrackController.addActivity(addActivityRequestDtoList);

		assertEquals("success", result);
	}

	@Mock
	FetchActivityTrackService fetchActivityTrackService;

	@Test
	@DisplayName("fetchallActivity")
	public void fetchallActivityTest() {

		when(fetchActivityTrackService.fetchallActivity(pageNumber, pageSize)).thenReturn(fetchActivityResponseDtoList);
		List<FetchActivityResponseDto> resultList = activityTrackController.fetchallActivity(pageNumber, pageSize);

		assertEquals(fetchActivityResponseDtoList, resultList);
	}

	@Test
	@DisplayName("fetchActivityWithEmployeecode")
	public void fetchActivityWithEmployeecodeTest() {

		when(fetchActivityTrackService.fetchActivityWithEmployeecode(pageNumber, pageSize, employeeCode))
				.thenReturn(fetchActivityResponseDtoList);
		List<FetchActivityResponseDto> employeecodeList = activityTrackController
				.fetchActivityWithEmployeecode(pageNumber, pageSize, employeeCode);

		assertEquals(fetchActivityResponseDtoList, employeecodeList);

	}

	@Test
	@DisplayName("fetchActivityWithDate")
	public void fetchActivityWithDate() {

		when(fetchActivityTrackService.fetchActivityWithDate(pageNumber, pageSize, activityDate))
				.thenReturn(fetchActivityResponseDtoList);
		List<FetchActivityResponseDto> dateList = activityTrackController.fetchActivityWithDate(pageNumber, pageSize,
				activityDate);
		assertEquals(fetchActivityResponseDtoList, dateList);

	}

	@Test
	@DisplayName("fetchActivityWithFromAndToDate")
	public void fetchActivityWithFromAndToDate() {

		when(fetchActivityTrackService.fetchActivityWithEmployeecodeFromAndTodate(pageNumber, pageSize, employeeCode,
				fromDate, toDate)).thenReturn(fetchActivityResponseDtoList);
		List<FetchActivityResponseDto> alldateList = activityTrackController
				.fetchActivityWithEmployeecodeFromAndTodate(pageNumber, pageSize, employeeCode, fromDate, toDate);
		assertEquals(fetchActivityResponseDtoList, alldateList);
	}

	@Test
	@DisplayName("fetchActivityWithEmployeeCodeAndDate")
	public void fetchActivityWithEmployeeCodeAndDate() {

		when(fetchActivityTrackService.fetchActivityWithEmployeeCodeAndDate(pageNumber, pageSize, employeeCode,
				activityDate)).thenReturn(fetchActivityResponseDtoList);
		List<FetchActivityResponseDto> empcodedateList = activityTrackController
				.fetchActivityWithEmployeeCodeAndDate(pageNumber, pageSize, employeeCode, activityDate);

		assertEquals(fetchActivityResponseDtoList, empcodedateList);

	}

	@Test
	@DisplayName("fetchActivityWithEmployeecodeFromAndTodate")
	public void fetchActivityWithEmployeecodeFromAndTodate() {

		when(fetchActivityTrackService.fetchActivityWithEmployeecodeFromAndTodate(pageNumber, pageSize, employeeCode,
				fromDate, toDate)).thenReturn(fetchActivityResponseDtoList);
		List<FetchActivityResponseDto> empcodebothdate = activityTrackController
				.fetchActivityWithEmployeecodeFromAndTodate(pageNumber, pageSize, employeeCode, fromDate, toDate);

		assertEquals(fetchActivityResponseDtoList, empcodebothdate);
	}

}
