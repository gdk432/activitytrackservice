package com.activity.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ActvityReportResponeTest {

	static ActvityReportRespone actvityReportRespone;

	@BeforeAll
	public static void setup() {
		actvityReportRespone = new ActvityReportRespone();
		actvityReportRespone.setId(1L);
		actvityReportRespone.setEmployeeName("dev");
		actvityReportRespone.setEmployeeCode(12354L);
		actvityReportRespone.setActivityDescription("for testing");
		actvityReportRespone.setActivityStatus("completed");
	}

	@Test
	public void getEmployeeNameTest() {
		String result = actvityReportRespone.getEmployeeName();
		assertEquals("dev", result);

	}

	@Test
	public void getEmployeeIdTest() {
		Long result = actvityReportRespone.getId();
		assertEquals(1L, result);

	}
	
	@Test
	public void getEmployeeCodeTest() {
		Long result = actvityReportRespone.getEmployeeCode();
		assertEquals(12354L, result);

	}

	@Test
	public void getEmployeeDescriptionTest() {
		String result = actvityReportRespone.getActivityDescription();
		assertEquals("for testing", result);

	}
	
	@Test
	public void getActivityStatusTest() {
		String result = actvityReportRespone.getActivityStatus();
		assertEquals("completed", result);

	}

}
