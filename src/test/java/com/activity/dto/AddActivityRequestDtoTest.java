package com.activity.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AddActivityRequestDtoTest {

	static AddActivityRequestDto addActivity;

	@BeforeAll
	public static void setup() {
		addActivity = new AddActivityRequestDto();
		addActivity.setEmployeeCode(123L);
		addActivity.setActivityDescription("testing");
		addActivity.setActivityStatus("completed");

	}

	@Test
	public void getEmployeeCodeTest() {
		Long result = addActivity.getEmployeeCode();
		assertEquals(123L, result);
	}

	@Test
	public void getDescriptionTest() {
		String result = addActivity.getActivityDescription();
		assertEquals("testing", result);
	}
	
	@Test
	public void getActivityStatusTest() {
		String result = addActivity.getActivityStatus();
		assertEquals("completed", result);
	}

}
