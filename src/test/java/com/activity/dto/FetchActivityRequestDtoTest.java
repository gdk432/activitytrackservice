package com.activity.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class FetchActivityRequestDtoTest {

	static FetchActivityRequestDto fetchActivity;

	@BeforeAll
	public static void setup() {
		fetchActivity = new FetchActivityRequestDto();
		fetchActivity.setEmployeeCode(123L);

	}
	@Test
	public void getEmployeeCodeTest() {
		Long result = fetchActivity.getEmployeeCode();
		assertEquals(123L, result);
	}

}
