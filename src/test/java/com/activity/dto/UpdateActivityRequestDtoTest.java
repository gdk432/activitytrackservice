package com.activity.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UpdateActivityRequestDtoTest {

	static UpdateActivityRequestDto updateActivity;

	@BeforeAll
	public static void setup() {
		updateActivity = new UpdateActivityRequestDto();
		updateActivity.setId(12L);
		updateActivity.setActivityStatus("completed");

	}

	@Test
	public void getIdTest() {
		Long result = updateActivity.getId();
		assertEquals(12L, result);

	}

	@Test
	public void getActivityStatusTest() {
		String result = updateActivity.getActivityStatus();
		assertEquals("completed", result);

	}

}
