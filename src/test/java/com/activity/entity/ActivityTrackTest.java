package com.activity.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.BeforeAll;

@ExtendWith(MockitoExtension.class)
public class ActivityTrackTest {

	static ActivityTrack activityTrack;
	static LocalDate activityDate = LocalDate.now();

	@BeforeAll
	public static void setup() {
		activityTrack = new ActivityTrack();
		activityTrack.setId(123L);
		activityTrack.setEmployeeCode(518485L);
		activityTrack.setActivityDate(activityDate);
		activityTrack.setActivityStatus("active");
		activityTrack.setActivityDescription("testing");

	}

	@Test
	public void getId() {
		Long result = activityTrack.getId();
		assertEquals(123L, result);

	}

}
